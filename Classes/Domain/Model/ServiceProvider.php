<?php

namespace BueroDigitale\NewsletterSubscriberSync\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class ServiceProvider extends AbstractEntity {
	protected $title = '';
	protected $apiCredentials = [];

	/**
	 * ServiceProvider constructor.
	 *
	 * @param string $title
	 * @param array  $apiCredentials
	 */
	public function __construct($title, array $apiCredentials) {
		$this->setTitle($title);
		$this->setApiCredentials($apiCredentials);
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * @return array
	 */
	public function getApiCredentials() {
		return $this->apiCredentials;
	}

	/**
	 * @param array $apiCredentials
	 */
	public function setApiCredentials($apiCredentials) {
		$this->apiCredentials = $apiCredentials;
	}


}