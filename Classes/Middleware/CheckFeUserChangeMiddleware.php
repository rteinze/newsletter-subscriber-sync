<?php
declare(strict_types=1);

namespace BueroDigitale\NewsletterSubscriberSync\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CheckFeUserChangeMiddleware implements MiddlewareInterface
{
	/**
	 * Adds an instance of TYPO3\CMS\Core\Http\NormalizedParams as
	 * attribute to $request object
	 *
	 * @param ServerRequestInterface  $request
	 * @param RequestHandlerInterface $handler
	 *
	 * @return ResponseInterface
	 */

	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		// get actions
		$userActions = $this->getActionsNormalized($request);


		// check response
		$response = $handler->handle($request);

		// test
		//file_put_contents('/usr/www/users/bd1184a/ftb2019_DEV/logfile-response-0.php', var_export($response,true) . "\n;\n", FILE_APPEND);

		return $response;
	}


	/**
	 * @param $request
	 *
	 * @return array
	 */
	protected function getActionsNormalized ($request) {
		$userActions = [];
		// test
		//file_put_contents('/usr/www/users/bd1184a/ftb2019_DEV/logfile-request-0.php', var_export($request, true) . "\n;\n", FILE_APPEND);

		// request
		$serverParams = $request->getServerParams();

		// get submitted actions
		$feUsers =
			($request->getQueryParams()['cmd']['fe_users'] ?: []) // delete
			+
			($request->getQueryParams()['data']['fe_users'] ?: []) // disable
			+
			($request->getQueryParams()['edit']['fe_users'] ?: []) // edit + new
		;

		if ($feUsers) {
			// test
			//file_put_contents('/usr/www/users/bd1184a/ftb2019_DEV/logfile-request-2.php', var_export($feUsers, true) . "\n;\n", FILE_APPEND);

			// form has been saved or flagged
			$isSaved = !!$request->getParsedBody()['doSave'];
			//$hasBody = !!$request->getParsedBody();

			// collect delete/new/update
			foreach ($feUsers as $feUserUid => $feUserActions) {
				// normalize
				$feUserActions = is_array($feUserActions) ? array_keys($feUserActions) : [$feUserActions];

				foreach ($feUserActions as $singleUserAction) {
					if ($isSaved || (in_array($singleUserAction, ['delete','disable']))) {
						$userActions[] = [
							'fe_user_uid'        => 'new' === $singleUserAction ? 0 : $feUserUid,
							'action'             => $singleUserAction,
							'request_time_float' => $serverParams['REQUEST_TIME_FLOAT']
						];
					}
				}
			}

			// really changed elements
			if ($userActions) {
				//file_put_contents('/usr/www/users/bd1184a/ftb2019_DEV/logfile-request-3.php', var_export($userActions, true) . "\n;\n", FILE_APPEND);
			}
		}

		return $userActions;
	}

}
