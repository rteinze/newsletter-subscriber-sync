<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'Newsletter Subscriber Synchronization',
	'description' => 'Synchronization of local FE-users with services like Mailchimp, Newsletter2Go, CleverReach, SendInBlue, ActiveCampaign, GetResponse und Klick-Tipp, rapidmail.',
	'category' => 'plugin',
	'author' => 'René Teinze',
	'author_company' => 'buero-digitale.de',
	'author_email' => 'r.teinze@buero-digitale.de',
	'state' => 'alpha',
	'clearCacheOnLoad' => true,
	'version' => '0.0.0',
	'constraints' => [
		'depends' => [
			'typo3' => '9.5.0-9.5.99',
		],
	],
];

#$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(Dispatcher::class);
#$signalSlotDispatcher->dispatch(CLASS, 'DoSomeThing', [$customValue1, $customValue2]);