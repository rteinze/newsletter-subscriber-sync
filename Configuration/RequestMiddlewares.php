<?php
return [
	'backend' => [
		'buerodigitale/newsletter-subscriber-sync/get-feuser-actions' => [
			'target' => BueroDigitale\NewsletterSubscriberSync\Middleware\CheckFeUserChangeMiddleware::class
			//, 'after' => ['typo3/cms-backend/locked-backend']
		]
	]
];
