<?php
return [
	'ctrl' => [
		'title' => 'LLL:EXT:newsletter_subscriber_sync/Resources/Private/Language/locallang.xlf:tx_newslettersubscribersync_domain_model_serviceprovider',
		'label' => 'name',
		'iconfile' => 'EXT:newsletter_subscriber_sync/Resources/Public/Icons/ServiceProvider.png',
	],
	'columns' => [
		'title' => [
			'label' => 'LLL:EXT:newsletter_subscriber_sync/Resources/Private/Language/locallang.xlf:tx_newslettersubscribersync_domain_model_serviceprovider.title',
			'config' => [
				'type' => 'input',
				'size' => '30',
				'eval' => 'trim',
			],
		],
		'apiCredentials' => [
			'label' => 'LLL:EXT:newsletter_subscriber_sync/Resources/Private/Language/locallang.xlf:tx_newslettersubscribersync_domain_model_serviceprovider.api_credentials',
			'config' => [
				'type' => 'text',
				'eval' => 'trim',
			],
		],
	],
	'types' => [
		'0' => ['showitem' => 'title, apiCredentials'],
	],
];